﻿CREATE TABLE Good_has_Facilities 
(
  Good_IdGood INTEGER  NOT NULL,
  Facilities_IdFacilities INTEGER  NOT NULL ,
    PRIMARY KEY(Good_IdGood, Facilities_IdFacilities),
    FOREIGN KEY(Good_IdGood) REFERENCES Good(IdGood),
    FOREIGN KEY(Facilities_IdFacilities) REFERENCES Facilities (IdFacilities),
)



