﻿CREATE TABLE Member 
(
  IdMember INTEGER  NOT NULL   IDENTITY(1,1),
  Last_name VARCHAR(45) NOT NULL,
  First_name VARCHAR(45) NOT NULL ,
  Email VARCHAR(45) NOT NULL ,
  Country VARCHAR(45) NOT NULL ,
  Phone VARCHAR(45) NOT NULL,
  AcceptCon BIT NOT NULL,
  GoogleAuth NVARCHAR(250),
  FacebookAuth NVARCHAR(250),
  MemberLogin  VARCHAR(16) NOT NULL, 
  MemberPassword VARBINARY(32) NOT NULL, 
  PRIMARY KEY(IdMember),
)
