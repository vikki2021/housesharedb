﻿CREATE TABLE Facilities  
(
  IdFacilities INTEGER  NOT NULL   IDENTITY(1,1) ,
  Garden BIT,
  Swimmingpool BIT,
  WashingMachine BIT,
  Internet BIT,
  AllowPet BIT,
PRIMARY KEY(IdFacilities),
FOREIGN KEY(Good_IdGood) REFERENCES Good(IdGood),
)
