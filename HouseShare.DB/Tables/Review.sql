﻿CREATE TABLE Review 
(
  IdReview INTEGER  NOT NULL IDENTITY ,
  Good_IdGood INTEGER  NOT NULL,
  Member_IdMember INTEGER  NOT NULL,
  Review VARCHAR(1024),
  Score INT CHECK (Score BETWEEN 0 AND 10),
  Validate_status BIT,
    PRIMARY KEY(IdReview),
    FOREIGN KEY(Member_IdMember)REFERENCES Member(IdMember),
    FOREIGN KEY(Good_IdGood)REFERENCES Good(IdGood),
  )










