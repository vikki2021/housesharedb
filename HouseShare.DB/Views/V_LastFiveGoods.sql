﻿--View:Les 5 derniers biens ajoutés en échanges--

CREATE VIEW [dbo].[V_LastFiveGoods]

	AS SELECT * FROM Good ORDER BY AvailableDate FETCH FIRST 5 ROWS ONLY;

	GO