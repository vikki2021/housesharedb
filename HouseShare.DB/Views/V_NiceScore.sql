﻿
--view:Les biens échangés ayant la meilleure réputation (avis > 6/10)--
CREATE VIEW [dbo].[V_NiceScore]
	AS SELECT Good.Title FROM Good INNER JOIN Review ON Good.IdGood= Review.Good_IdGood WHERE Review.Score>=6
	GO